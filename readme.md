## Gitlab form enrichment

Browser extension to add common form elements (dropdown, checkbox, date-picker) for gitlab pipeline variables.

Extension uses Description of pipeline variable and looks for hints on how to display. Supported hints:

- [x] `@@list[apple, banana, orage]` - transforms variable value input to a dropdown. If value is specified for the variable in yaml file, it will be used as a default. If no value is provided by default, dropdown would provide a "placeholder" (empty default value).
- [x] `@@bool` - transforms variable to a checkbox. Sets value to "true" or "false" depending on selection. If value is specified for the variable in yaml file ("true" or "false", case insensitive), it would be used as initial value.
- [x] `@@date` - transforms variable to a date picker. Sets value in `YYYY-MM-DD` format (see [MDN docs](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date)). If value is specified for the variable in yaml file (in correct format), it would be used as initial value.
- [x] `@@datetime` - transforms variable to a datetime picker. Sets value in `YYYY-MM-DDTHH:MM` format (see [MDN docs](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/datetime-local)). If value is specified for the variable in yaml file (in correct format), it would be used as initial value.
- [x] `@@date-range` - transforms variable to a date-range picker. Sets value in `YYYY-MM-DD YYYY-MM-DD` format (start-date and end-date divided by space, see [MDN docs](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date)).
- [x] `@@datetime-range` - transforms variable to a datetime-range picker. Sets value in `YYYY-MM-DDTHH:MM YYYY-MM-DDTHH:MM` format (start-datetime and end-datetime divided by space, see [MDN docs](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/datetime-local) for datetime-local input)

---

This is a [Plasmo extension](https://docs.plasmo.com/) project bootstrapped with [`plasmo init`](https://www.npmjs.com/package/plasmo).

## Getting Started

First, run the development server:

```bash
pnpm dev
# or
npm run dev
```

Open your browser and load the appropriate development build. For example, if you are developing for the chrome browser, using manifest v3, use: `build/chrome-mv3-dev`.

You can start editing the popup by modifying `popup.tsx`. It should auto-update as you make changes. To add an options page, simply add a `options.tsx` file to the root of the project, with a react component default exported. Likewise to add a content page, add a `content.ts` file to the root of the project, importing some module and do some logic, then reload the extension on your browser.

For further guidance, [visit our Documentation](https://docs.plasmo.com/)

## Making production build

Run the following:

```bash
pnpm build
# or
npm run build
```

This should create a production bundle for your extension, ready to be zipped and published to the stores.

## Submit to the webstores

The easiest way to deploy your Plasmo extension is to use the built-in [bpp](https://bpp.browser.market) GitHub action. Prior to using this action however, make sure to build your extension and upload the first version to the store to establish the basic credentials. Then, simply follow [this setup instruction](https://docs.plasmo.com/workflows#submit-your-extension) and you should be on your way for automated submission!
