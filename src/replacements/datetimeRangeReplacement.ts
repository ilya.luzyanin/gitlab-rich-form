import type { FormVariable } from "../formVariable"
import { Replacement } from "./replacement"

class DatetimeRangeReplacement extends Replacement {
  constructor() {
    super(/@@datetime-range/)
  }

  createReplacement(formVariable: FormVariable) {
    const startDatetimeElement = document.createElement("input")
    startDatetimeElement.type = "datetime-local"
    startDatetimeElement.className =
      "gl-form-input form-control gl-mb-3 gl-mr-3"

    const endDatetimeElement = document.createElement("input")
    endDatetimeElement.type = "datetime-local"
    endDatetimeElement.className = "gl-form-input form-control gl-mb-3"

    const changeHandler = () => {
      formVariable.valueElement.value = `${startDatetimeElement.value} ${endDatetimeElement.value}`
      formVariable.valueElement.dispatchEvent(new Event("input"))
    }
    startDatetimeElement.addEventListener("change", changeHandler)
    endDatetimeElement.addEventListener("change", changeHandler)

    const rangeDivElement = document.createElement("div")
    rangeDivElement.style.display = "flex"
    rangeDivElement.style.width = "100%"

    rangeDivElement.append(startDatetimeElement, endDatetimeElement)

    return rangeDivElement
  }
}

export default DatetimeRangeReplacement
