import type { FormVariable } from "../formVariable"
import { Replacement } from "./replacement"

class DateReplacement extends Replacement {
  constructor() {
    super(/@@date(?:\s|$)/)
  }

  createReplacement(formVariable: FormVariable) {
    const dateRegex = /\d{4}-\d{2}-\d{2}/

    const dateElement = document.createElement("input")
    dateElement.type = "date"
    dateElement.className = "gl-form-input form-control gl-mb-3"

    if (dateRegex.test(formVariable.value)) {
      dateElement.value = formVariable.value
    }

    dateElement.addEventListener("change", () => {
      formVariable.valueElement.value = dateElement.value
      formVariable.valueElement.dispatchEvent(new Event("input"))
    })

    return dateElement
  }
}

export default DateReplacement
