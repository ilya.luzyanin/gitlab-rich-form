import type { FormVariable } from "../formVariable"
import { Replacement } from "./replacement"

class BoolReplacement extends Replacement {
  constructor() {
    super(/@@bool/)
  }

  createReplacement(formVariable: FormVariable) {
    const checkboxElement = document.createElement("input")
    checkboxElement.type = "checkbox"
    checkboxElement.className = "form-control gl-mb-3"

    if (formVariable.value?.toLowerCase() === "true") {
      checkboxElement.checked = true
    }

    checkboxElement.addEventListener("change", () => {
      formVariable.valueElement.value = checkboxElement.checked
        ? "true"
        : "false"
      formVariable.valueElement.dispatchEvent(new Event("input"))
    })

    return checkboxElement
  }
}

export default BoolReplacement
