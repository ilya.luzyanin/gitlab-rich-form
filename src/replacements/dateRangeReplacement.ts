import type { FormVariable } from "../formVariable"
import { Replacement } from "./replacement"

class DateRangeReplacement extends Replacement {
  constructor() {
    super(/@@date-range/)
  }

  createReplacement(formVariable: FormVariable) {
    const startDateElement = document.createElement("input")
    startDateElement.type = "date"
    startDateElement.className = "gl-form-input form-control gl-mb-3 gl-mr-3"

    const endDateElement = document.createElement("input")
    endDateElement.type = "date"
    endDateElement.className = "gl-form-input form-control gl-mb-3"

    const changeHandler = () => {
      formVariable.valueElement.value = `${startDateElement.value} ${endDateElement.value}`
      formVariable.valueElement.dispatchEvent(new Event("input"))
    }
    startDateElement.addEventListener("change", changeHandler)
    endDateElement.addEventListener("change", changeHandler)

    const rangeDivElement = document.createElement("div")
    rangeDivElement.style.display = "flex"
    rangeDivElement.style.width = "100%"

    rangeDivElement.append(startDateElement, endDateElement)

    return rangeDivElement
  }
}

export default DateRangeReplacement
