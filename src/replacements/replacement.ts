import type { FormVariable } from "../formVariable"

export abstract class Replacement {
  regex: RegExp
  constructor(regex) {
    this.regex = regex
  }

  isMatch(formVariable: FormVariable): boolean {
    return this.regex.test(formVariable.description)
  }

  protected abstract createReplacement(formVariable: FormVariable): HTMLElement

  replace(formVariable: FormVariable) {
    formVariable.valueElement.style.width = "0"
    formVariable.valueElement.style.padding = "0"

    const replacement = this.createReplacement(formVariable)

    formVariable.valueElement.parentNode.insertBefore(
      replacement,
      formVariable.valueElement
    )
  }
}
