import type { FormVariable } from "../formVariable"
import { Replacement } from "./replacement"

class ListReplacement extends Replacement {
  constructor() {
    super(/@@list\[(.+(?:,\s*.+)*)\]/)
  }

  createReplacement(formVariable: FormVariable) {
    const options = formVariable.description
      .match(this.regex)[1]
      .split(",")
      .map((v) => v.trim())
    let selectedOption = formVariable.value
    if (selectedOption && !options.includes(selectedOption)) {
      selectedOption = options[0]
    }

    const selectElement = document.createElement("select")
    selectElement.className = "gl-form-select custom-select gl-mb-3"
    options.forEach((option) => {
      const optionElement = document.createElement("option")
      optionElement.value = option
      optionElement.text = option
      if (option === selectedOption) {
        optionElement.selected = true
      }
      selectElement.appendChild(optionElement)
    })

    if (!selectedOption) {
      const defaultOption = document.createElement("option")
      defaultOption.hidden = true
      defaultOption.disabled = true
      defaultOption.selected = true
      defaultOption.text = "-- Select a value --"
      selectElement.prepend(defaultOption)
    }

    selectElement.addEventListener("change", () => {
      formVariable.valueElement.value = selectElement.value
      formVariable.valueElement.dispatchEvent(new Event("input"))
    })

    return selectElement
  }
}

export default ListReplacement
