import type { FormVariable } from "../formVariable"
import { Replacement } from "./replacement"

class DatetimeReplacement extends Replacement {
  constructor() {
    super(/@@datetime(?:\s|$)/)
  }

  createReplacement(formVariable: FormVariable) {
    const datetimeRegex = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/

    const datetimeElement = document.createElement("input")
    datetimeElement.type = "datetime-local"
    datetimeElement.className = "gl-form-input form-control gl-mb-3"

    if (datetimeRegex.test(formVariable.value)) {
      datetimeElement.value = formVariable.value
    }

    datetimeElement.addEventListener("change", () => {
      formVariable.valueElement.value = datetimeElement.value
      formVariable.valueElement.dispatchEvent(new Event("input"))
    })

    return datetimeElement
  }
}

export default DatetimeReplacement
