export const waitForElement = (selector: string): Promise<Element> => {
  return new Promise((resolve) => {
    const existingNode = document.querySelector(selector)
    if (existingNode) {
      return resolve(existingNode)
    }

    const observer = new MutationObserver((mutations) => {
      let node: Element = undefined
      mutations.find((mutation) => {
        if (mutation.type !== "childList") {
          return false
        }
        for (let addedNode of mutation.addedNodes) {
          const element = addedNode as Element
          if (element && element.matches(selector)) {
            node = element
            return true
          }
        }
      })
      if (node) {
        resolve(node)
        observer.disconnect()
      }
    })

    observer.observe(document.body, {
      childList: true,
      subtree: true
    })
  })
}
