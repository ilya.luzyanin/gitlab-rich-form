export type FormVariable = {
  name: string
  value: string
  description: string
  valueElement: HTMLTextAreaElement
}
