import type { PlasmoContentScript } from "plasmo"

import type { FormVariable } from "./formVariable"
import BoolReplacement from "./replacements/boolReplacement"
import DateRangeReplacement from "./replacements/dateRangeReplacement"
import DateReplacement from "./replacements/dateReplacement"
import DatetimeRangeReplacement from "./replacements/datetimeRangeReplacement"
import DatetimeReplacement from "./replacements/datetimeReplacement"
import ListReplacement from "./replacements/listReplacement"
import { waitForElement } from "./waitForElement"

const variableRowSelector = '[data-testid="ci-variable-row-container"]'
const replacements = [
  new ListReplacement(),
  new BoolReplacement(),
  new DateReplacement(),
  new DateRangeReplacement(),
  new DatetimeReplacement(),
  new DatetimeRangeReplacement()
]

export const config: PlasmoContentScript = {
  matches: ["*://*/*/-/pipelines/new"]
}

const parseVariables = (): FormVariable[] => {
  const variableRowElements = document.querySelectorAll(variableRowSelector)
  const variables = []
  variableRowElements.forEach((rowElement) => {
    if (rowElement.children.length > 1) {
      const variableName = (
        rowElement.children[0].children[1] as HTMLInputElement
      ).value
      const variableValue = (
        rowElement.children[0].children[2] as HTMLInputElement
      ).value
      const variableDescription = rowElement.children[1].textContent.trim()
      const valueElement = rowElement.children[0].children[2]
      variables.push({
        valueElement,
        name: variableName,
        value: variableValue,
        description: variableDescription
      })
    }
  })

  return variables
}

const enrichForm = (variableElements: FormVariable[]) => {
  for (const variableElement of variableElements) {
    const replacement = replacements.find((r) => r.isMatch(variableElement))
    if (replacement) {
      replacement.replace(variableElement)
    }
  }
}

window.addEventListener("load", async () => {
  await waitForElement(variableRowSelector)
  const variables = parseVariables()

  enrichForm(variables)
})
